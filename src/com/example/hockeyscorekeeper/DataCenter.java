package com.example.hockeyscorekeeper;

import java.util.HashMap;
import java.util.List;

import android.util.SparseIntArray;

public class DataCenter {
//	static SparseIntArray team1Shots = new SparseIntArray();
//	static SparseIntArray team2Shots = new SparseIntArray();
//	
	static HashMap<Integer,Integer> team1shots=new HashMap<Integer, Integer>();
	static HashMap<Integer,Integer> team2shots=new HashMap<Integer, Integer>();
	
	static int team1Goals;
	static int team2Goals;
	static List<GoalInfo> goalRegister;
	static List<String> playerNames;
	static List<String> goalieNames;
	
	class GoalInfo{
		String shooter;
		String assist1;
		String assist2;
		String against;
		int period;
	}

}
