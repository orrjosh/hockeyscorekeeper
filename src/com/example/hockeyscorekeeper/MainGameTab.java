package com.example.hockeyscorekeeper;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MainGameTab extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View mainInflater =inflater.inflate(R.layout.main_game_tab, null,false);
		return mainInflater;
	}
}



