package com.example.hockeyscorekeeper;

import java.util.HashMap;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.*;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener{
	private RadioButton period1,period2,period3,periodOT;
	private TextView shot1Period, shot2Period, shot1Game, shot2Game, team1goals, team2goals;
	private RadioGroup periodGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		
		period1=(RadioButton)findViewById(R.id.period1);
		
		period2=(RadioButton)findViewById(R.id.period2);
		period3=(RadioButton)findViewById(R.id.period3);
		periodOT=(RadioButton)findViewById(R.id.periodOT);
		shot1Period = (TextView)findViewById(R.id.shot1Period);
		shot2Period = (TextView)findViewById(R.id.shot2Period);
		shot1Game = (TextView)findViewById(R.id.shot1Game);
		shot2Game = (TextView)findViewById(R.id.shot2Game);
		team1goals= (TextView)findViewById(R.id.team1Goals);
		team2goals= (TextView)findViewById(R.id.team2Goals);
		periodGroup= (RadioGroup)findViewById(R.id.periodGroup);
		periodGroup.setOnCheckedChangeListener(this);
		
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void shotClicked(View v){
		int periodID=0;
		if(period1.isChecked()){
			periodID=1;
		}
		if(period2.isChecked()){
			periodID=2;
		}
		if(period3.isChecked()){
			periodID=3;
		}
		if(periodOT.isChecked()){
			periodID=4;
		}
		if(v.getId()==R.id.shot1){
			int tempShots=0;
			if(DataCenter.team1shots.containsKey(periodID)){
				tempShots = DataCenter.team1shots.get(periodID);
			}
			tempShots++;
			DataCenter.team1shots.put(periodID, tempShots);
			shot1Period.setText(String.valueOf(tempShots));
			shot1Game.setText(String.valueOf(calcTotalShots(DataCenter.team1shots)));
		}
		if(v.getId()==R.id.shot2){
			int tempShots=0;
			if(DataCenter.team2shots.containsKey(periodID)){
				tempShots = DataCenter.team2shots.get(periodID);
			}
			tempShots++;
			DataCenter.team2shots.put(periodID, tempShots);
			shot2Period.setText(String.valueOf(tempShots));
			shot2Game.setText(String.valueOf(calcTotalShots(DataCenter.team2shots)));
		}
		
	}
	public void goalClicked(View v){
		
	}
	
	public int calcTotalShots(HashMap<Integer,Integer> teamShotMap){
		int shots=0;
		for(Integer key:teamShotMap.keySet()){
			shots+=teamShotMap.get(key);
		}
		return shots;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if(period1.isChecked()){
			if(!DataCenter.team1shots.containsKey(1)){
				DataCenter.team1shots.put(1, 0);
			}
			shot1Period.setText(""+DataCenter.team1shots.get(1));
			if(!DataCenter.team2shots.containsKey(1)){
				DataCenter.team2shots.put(1, 0);
			}
			shot2Period.setText(""+DataCenter.team2shots.get(1));
		}else if(period2.isChecked()){
			if(!DataCenter.team1shots.containsKey(2)){
				DataCenter.team1shots.put(2, 0);
			}
			shot1Period.setText(""+DataCenter.team1shots.get(2));
			if(!DataCenter.team2shots.containsKey(2)){
				DataCenter.team2shots.put(2, 0);
			}
			shot2Period.setText(""+DataCenter.team2shots.get(2));
		}else if(period3.isChecked()){
			if(!DataCenter.team1shots.containsKey(3)){
				DataCenter.team1shots.put(3, 0);
			}
			shot1Period.setText(""+DataCenter.team1shots.get(3));
			if(!DataCenter.team2shots.containsKey(3)){
				DataCenter.team2shots.put(3, 0);
			}
			shot2Period.setText(""+DataCenter.team2shots.get(3));
			
		}else if(periodOT.isChecked()){
			if(!DataCenter.team1shots.containsKey(4)){
				DataCenter.team1shots.put(4, 0);
			}
			shot1Period.setText(""+DataCenter.team1shots.get(4));
			if(!DataCenter.team2shots.containsKey(4)){
				DataCenter.team2shots.put(4, 0);
			}
			shot2Period.setText(""+DataCenter.team2shots.get(4));
			
		}
		
	}

}


//private FragmentTabHost mTabHost;
//private Clock_Tab clockTab;
//
//@Override
//protected void onCreate(Bundle savedInstanceState) {
//	super.onCreate(savedInstanceState);
//	setContentView(R.layout.activity_main);
//	mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
//	mTabHost.setup(this,getSupportFragmentManager(),R.id.realtabcontent);
//	
//	mTabHost.addTab(mTabHost.newTabSpec("Clock").setIndicator("Clock"), Clock_Tab.class,null);
//	mTabHost.addTab(mTabHost.newTabSpec("Button").setIndicator("Button"), Button_Tab.class,null);
//	mTabHost.addTab(mTabHost.newTabSpec("Spinner").setIndicator("Spinner"), Spinner_Tab.class,null);
//	mTabHost.addTab(mTabHost.newTabSpec("Switch").setIndicator("Switch"), Switch_Tab.class,null);
//	mTabHost.setCurrentTab(1);
//}